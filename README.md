# Demo operator

Simple demo operator that compute the mean.

# Create app and deploy

Clone the repo and build the package.

```
library(demooperator)
bntools::createApp(tags=c('Test'), mainCategory = 'Test')
bntools::deployApp()
```
```
bnshiny::startBNTestShiny('demooperator')
# see workspace.R for a full example
```
